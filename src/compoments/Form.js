import React, { Component } from 'react';
import './style.css'
class Form extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Name: '',
            Email: '',
            languague: 'java',
            About: ''
        }
    }

    handleNameChange = (event) => {
        this.setState({
            Name: event.target.value
        })
    }

    handleEmailChange = (event) => {
        this.setState({
            Email: event.target.value
        })
    }

    handleAboutChange = (event) => {
        this.setState({
            About: event.target.value
        })
    }

    handleSubmit = event => {
        alert('Form submitted')
        event.preventDefault()
    }

    render() {
        const {Name, Email, language, About} = this.state
        return (
            <form className='form' onSubmit={this.handleSubmit}>
                <div className='name'>
                    <label>Name </label>
                    <input type='text' value={Name} onChange={this.handleNameChange} required />
                </div>
                <div className='email'>
                    <label>Email ID </label>
                    <input type='email' value={Email} onChange={this.handleEmailChange} required />
                </div>
                <div className='language'>
                    <label>Preffered Languague </label>
                    <select value={language} required>
                        <option value="c++">C++</option>
                        <option value="java">Java</option>
                        <option value="python">Python</option>
                    </select>
                </div>
                <div className='about'>
                    <label>About </label>
                    <textarea type='text'
                        placeholder='Tell us about yourself'
                        value={About}
                        onChange={this.handleAboutChange} required />
                </div>
                <button type='submit'>Submit</button>
            </form>
        )
    }
}

export default Form