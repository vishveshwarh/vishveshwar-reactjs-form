import logo from './logo.svg';
import './App.css';
import Form from './compoments/Form';

function App() {
  return (
    <div className="App">
      <Form />
    </div>
  );
}

export default App;
